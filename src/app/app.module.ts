import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MockService } from './mockService';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { CategoryButtonComponent } from './category.component';
import { ProductComponent } from './product.component';
import { ShoppingCartHeaderComponent } from './shoppingCartHeader.component';
import { DashboardComponent } from './dashboard.component';
import { ShoppingCartComponent } from './shoppingCart.component';

import { OrderService } from './orderService';
import { FormComponent } from './form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DeploydService } from './deploydService';
import { FilterComponent } from './filter.component';
import { RegexFilterPipe, FromPriceFilterPipe, ToPriceFilterPipe } from './filter.pipe';

import { NotificationsModule, NotificationsService } from 'angular4-notify';

@NgModule({
  declarations: [
    AppComponent,
    CategoryButtonComponent,
    ProductComponent,
    ShoppingCartHeaderComponent,
    DashboardComponent,
    ShoppingCartComponent,
    FormComponent,
    FilterComponent,
    RegexFilterPipe,
    FromPriceFilterPipe,
    ToPriceFilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgxPaginationModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NotificationsModule
  ],
  providers: [ MockService, OrderService, DeploydService, NotificationsService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
