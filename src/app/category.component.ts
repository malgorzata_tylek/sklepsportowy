import { Component, Input } from '@angular/core';
import { Category } from './category';

@Component({
    selector: 'CategoryButtonComponent',
    styleUrls: ['./category.component.css'],
    template:
    `
      <button class="category-button roboto-light"
              [ngClass]="{'selected': isSelected}" 
              (click)="showProducts()">{{category.name}}</button>
    `
})


export class CategoryButtonComponent {
    @Input() category: Category;
    isSelected: Boolean = false;

    showProducts() {
      console.log('Products ' + this.category.id + ' shown');
    }

    toggleSelection(categoryID: number): void {
      this.isSelected = categoryID === this.category.id;
    }
}
