import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Category } from './category';
import { MockService } from './mockService';
import { ShoppingCartHeaderComponent } from './shoppingCartHeader.component';
import { OrderService } from './orderService';
import { DeploydService } from './deploydService';
import { CategoryButtonComponent } from './category.component';
import { FilterComponent } from './filter.component';

@Component({
    selector: `DashboardComponent`,
    styleUrls: ['./dashboard.component.css'],
    template: `
    <div id="title-header">
      <div>{{title | uppercase}}</div>
      <ShoppingCartHeaderComponent id="shopping-cart" class="shopping-cart-header"> </ShoppingCartHeaderComponent>
    </div>
    <div class="main-container">
      <div class="filter-section">
        <FilterComponent class="filters"> </FilterComponent>
      </div>
      <div id="content">
         <div id="menu">
           <button class="main-page-button roboto-regular"
                   (click)="showMainPage()">Strona główna</button>
           <CategoryButtonComponent
             #categories
             *ngFor="let singleCategory of categories"
             [category]=singleCategory
             (click)="showProductsOfCategory(singleCategory.id)" >
           </CategoryButtonComponent>
         </div>
         <div id="products">
           <ProductComponent
             class="product"
             (updateCartEmitter)="getUpdateCartEvent($event)"
             *ngFor="let singleProduct of selectedCategory.products 
              | regexPriceFilter: nameRegex 
              | fromPriceFilter: fromPrice
              | toPriceFilter: toPrice
              | paginate: {itemsPerPage: 3, currentPage: p }"
             [product] = singleProduct >
           </ProductComponent>
           <div class="pagination-controls">
             <pagination-controls
               (pageChange)="p=$event"
               previousLabel=""
               nextLabel="" >
             </pagination-controls>
           </div>
         </div>
      </div>
    </div>
    `
})

export class DashboardComponent implements OnInit {
    @ViewChild(ShoppingCartHeaderComponent) cart: ShoppingCartHeaderComponent;
    @ViewChildren(CategoryButtonComponent) categoriesComponents: QueryList<CategoryButtonComponent>;
    @ViewChild(FilterComponent) filters: FilterComponent;

    title = 'sklep sportowy';
    categories: Category[];
    selectedCategory: Category;

    nameRegex: string;
    fromPrice: number;
    toPrice: number;

    constructor(private mockService: MockService, private orderService: OrderService, private deploydService: DeploydService) {
    }

    ngOnInit(): void {
        this.categories = [];
        // this.categories = this.mockService.getCategoriesData();

        this.deploydService.getCategoriesData()
            .then(function (data) {
                this.categories = data;
                this.showMainPage();
            }.bind(this));

        this.selectedCategory = this.categories[0];

        this.filters.sendPriceFilters
            .subscribe(({fromPrice, toPrice}) => {
                this.fromPrice = fromPrice;
                this.toPrice = toPrice;
            });

        this.filters.sendRegexFilters.subscribe(({regex}) => {
            this.nameRegex = regex;
        });
    }

    showMainPage(): void {
        this.selectedCategory = this.categories[0];
    }

    showProductsOfCategory(categoryID: number): void {
        this.selectedCategory = this.getCategoryById(categoryID);

        for (const category of this.categoriesComponents.toArray()) {
            category.toggleSelection(categoryID);
        }
    }

    getCategoryById(id: number): Category {
        const found = this.categories.filter((category) => category.id === id)[0];

        return found ? found : this.selectedCategory;
    }

    getUpdateCartEvent(product) {
        this.orderService.addProduct(product);
        this.cart.updateOrder(product);
    }
}
