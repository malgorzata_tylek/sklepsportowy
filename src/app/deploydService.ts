import {Injectable} from '@angular/core';
import {Category} from './category';

import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {Http, Headers, Response, RequestOptions} from '@angular/http';

@Injectable()
export class DeploydService {

    constructor(private http: Http) {
    }

    getCategoriesData(): Promise<Category[]> {
        return new Promise((resolve, reject) => {
            let data;
            this.getData()
                .subscribe((value) => {
                    data = value;
                    return data;
                }, function (err) {
                    reject({err, data});
                }, function () {
                    resolve(data);
                });
        });
    }

    getCategoriesData2(): Promise<Category[]> {
      return new Promise((resolve, reject) => {
        let data;
        this.getData2()
          .subscribe((value) => {
            data = value;
            return data;
          }, function (err) {
            console.warn('error', arguments);
            reject({err, data});
          }, function () {
            console.warn('complete', arguments, data);
            resolve(data);
          });
      });
    }

    getData2(): Observable<Category[]> {
      const apiUrl = 'http://localhost:3000/categories';

      return this.http.get(apiUrl)
        .map((response: Response) => {
          console.warn('response', response);
          return response.json();
        });

    }

    getData(): Observable<Category[]> {
        const apiUrl = 'http://localhost:4200/rest/categories';

        const headers = new Headers();
        headers.append('Cache-Control', 'no-cache');
        headers.append('Content-Type', 'application/json');
        headers.append('x-apikey', '5a0ef7411ef3dc24313a7d2e');

        const options = new RequestOptions({headers, withCredentials: true});
        // options.withCredentials = true;
        return this.http.get(apiUrl, options)
            .map((response: Response) => {
                console.warn('response', response);
                return response.json();
        });
    }

    sendOrder(data) {
      // todo post do bazy
      const apiUrl = 'http://localhost:4200/rest/orders';

      const headers = new Headers();
      headers.append('Cache-Control', 'no-cache');
      headers.append('Content-Type', 'application/json');
      headers.append('x-apikey', '5a0ef7411ef3dc24313a7d2e');

      const options = new RequestOptions({headers, withCredentials: true});

      return this.http.post(apiUrl, data, options)
        .subscribe(
          res => {
            console.log(res);
          },
          err => {
            console.log('Error occured');
          }
        );
    }
}
