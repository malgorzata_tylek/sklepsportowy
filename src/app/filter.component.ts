import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'FilterComponent',
  styleUrls: ['./filter.component.css'],
  template:
    `
      <div class="content">
        <div class="label-div medium-font roboto-regular">Filtruj:</div>
        <div id="filters" class="filters">
          <div id="price-filter">
            <div class="first-column">
              <input 
                id="price-checkbox"
                type="checkbox"
                [ngModel]="priceChecked"
                (ngModelChange)="onPriceFilterChange($event)"
              >po cenie
            </div>
            <div id="price-filter-details" class="second-column">
              <span>od:</span>
              <input
                type="number"
                [(ngModel)]="fromPrice"
                (ngModelChange)="onFromChange()">
              <span>do:</span>
              <input
                type="number"
                [(ngModel)]="toPrice"
                (ngModelChange)="onToChange()">
            </div>
          </div>
          <div id="regex-filter">
            <div class="first-column">
              <input 
                id="regex-checkbox"
                type="checkbox"
                [ngModel]="regexChecked"
                (ngModelChange)="onRegexFilterChange($event)"
              >po nazwie
            </div>
            <div id="regex-filter-details" class="second-column">
              <input
                class="regex-input"
                [(ngModel)]="regex"
                (ngModelChange)="onRegexChange()">
            </div>
          </div>
        </div>
      </div>
    `
})


export class FilterComponent {
  @Output() sendPriceFilters: EventEmitter<any> = new EventEmitter();
  @Output() sendRegexFilters: EventEmitter<any> = new EventEmitter();

  priceChecked: Boolean;
  regexChecked: Boolean;

  fromPrice: number;
  toPrice: number;
  regex: string;

  onPriceFilterChange(isChecked: boolean) {
    this.priceChecked = isChecked;
    this.emitIfChecked(this.sendPriceFilters, {fromPrice: this.fromPrice, toPrice: this.toPrice}, this.priceChecked);
    this.emitIfChecked(this.sendPriceFilters, {fromPrice: undefined, toPrice: undefined}, !this.priceChecked);
  }

  onRegexFilterChange(isChecked: boolean) {
    this.regexChecked = isChecked;
    this.emitIfChecked(this.sendRegexFilters, {regex: this.regex}, this.regexChecked);
    this.emitIfChecked(this.sendRegexFilters, {regex: undefined}, !this.regexChecked);
  }

  emitIfChecked(emitter, object, condition) {
    if (condition) {
      emitter.emit(object);
    }
  }

  onFromChange() {
    this.emitIfChecked(this.sendPriceFilters, {fromPrice: this.fromPrice, toPrice: this.toPrice}, this.priceChecked);
  }

  onToChange() {
    this.emitIfChecked(this.sendPriceFilters, {fromPrice: this.fromPrice, toPrice: this.toPrice}, this.priceChecked);
  }

  onRegexChange() {
    this.emitIfChecked(this.sendRegexFilters, {regex: this.regex}, this.regexChecked);
  }
}
