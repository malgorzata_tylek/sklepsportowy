import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fromPriceFilter'
})

export class FromPriceFilterPipe implements PipeTransform {
  transform(items: any[], priceFrom): any {
    console.log('priceFrom', priceFrom);

    return priceFrom
      ? items.filter(item => item.price >= priceFrom)
      : items;
  }
}

@Pipe({
  name: 'toPriceFilter'
})

export class ToPriceFilterPipe implements PipeTransform {
  transform(items: any[], priceTo): any {
    console.log('priceTo', priceTo);

    return priceTo
      ? items.filter(item => item.price >= priceTo)
      : items;
  }
}

@Pipe({
  name: 'regexPriceFilter'
})

export class RegexFilterPipe implements PipeTransform {
  transform(items: any[], regex): any {
    console.log('regexPriceFilter', regex);

    return regex
      ? items.filter(item => item.name.indexOf(regex) !== -1 || item.description.indexOf(regex) !== -1)
      : items;
  }
}
