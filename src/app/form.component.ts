import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NotificationsService } from 'angular4-notify';
import { OrderService } from './orderService';
import { DeploydService } from './deploydService';
import { Product } from './product';
import { Utils } from './utils';

class Order {
  id: number;
  amount: number;

  constructor(id, amount) {
    this.id = id;
    this.amount = amount;
  }
}

@Component({
  selector: `FormComponent`,
  styleUrls: ['./form.component.css'],
  template:
    `
    <div id="title-header">
      <div>{{title | uppercase}}</div>
      <ShoppingCartHeaderComponent id="shopping-cart" class="shopping-cart-header"> </ShoppingCartHeaderComponent>
    </div>
    <div class="main-container">
      <angular4-notify-notifications-container></angular4-notify-notifications-container>
      <div id="content">
          <div class="large-font margin-top">Odbiorca: </div>
          <div>Imię i nazwisko</div>
          <input
            type="text" 
            [(ngModel)]="name"
            (blur)="getName()">

          <div *ngIf="nameError" class="error">
            Name is required.
          </div>
            
          <div class="large-font">Adres: </div>
          <div>Nazwa ulicy:</div>
          <input           
            type="text"
            [(ngModel)]="address"
            (blur)="getAddress()">

          <div *ngIf="addressError" class="error">
            Name is required.
          </div>
        <div class="finish-button">
          <button
            (click)="finish()">
            Dokończ zamówienie
          </button>
        </div>
      </div>
    </div>
    `
})

export class FormComponent {
  title = 'sklep sportowy';
  name: string;
  address: string;

  form: FormGroup;
  nameError: boolean;
  addressError: boolean;

  constructor(protected notificationsService: NotificationsService,
              private orderService: OrderService, private deploydService: DeploydService) {}

  getName() {
    this.nameError = !this.validate(this.name);
  }

  getAddress() {
    this.addressError = !this.validate(this.address);
  }

  validate(value: string): boolean {
    return value !== '' && value !== undefined;
  }

  finish() {
    this.nameError = !this.validate(this.name);
    this.addressError = !this.validate(this.address);

    if (!this.nameError && !this.addressError) {
      this.notificationsService.addInfo('Dziękujemy za zakupy!');
      this.sendOrderToServer();
    }
  }

  sendOrderToServer() {
    const products = this.orderService.getCurrentOrder();
    let id, tmpProducts;
    let orders = new Array();

    for (const product of products) {
      id = product.id;
      tmpProducts = Utils._filterById(products, id);

      if (Utils._filterById(orders, id).length === 0) {
        orders.push(new Order(id, tmpProducts.length));
      }
    }

    const data = {
      name: this.name,
      address: this.address,
      totalValue: this.orderService.totalPrice,
      cart: JSON.stringify(orders)
    };

    this.deploydService.sendOrder(data);
  }
}
