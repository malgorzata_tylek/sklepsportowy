import { Product } from './product';
import { Category } from './category';

export const CATEGORIES: Category[] = [
  {
    id: 1,
    name: "Slackline",
    products: [
      {
        id: 11,
        name: "PurpleMonster",
        description: "Taśma Slackline 25m",
        price: 190,
        currency: "PLN"
      },
      {
        id: 12,
        name: "Blue Mile",
        description: "Taśma Slackline 97m",
        price: 385,
        currency: "PLN"
      },
      {
        id: 13,
        name: "Slamina",
        description: "Taśma Slackline - ProTrickline",
        price: 275,
        currency: "PLN"
      }
    ]
  },
  {
    id: 2,
    name: "Taniec brzucha",
    products: [
      {
        id: 21,
        name: "Cairo",
        description: "Baletki białe",
        price: 49.90,
        currency: "PLN"
      },
      {
        id: 22,
        name: "Pink Cairo",
        description: "Baletki różowe",
        price: 49.90,
        currency: "PLN"
      },
      {
        id: 23,
        name: "Modern",
        description: "Baletki czarne",
        price: 49.90,
        currency: "PLN"
      },
      {
        id: 24,
        name: "Thong",
        description: "Ochraniacze na śródstopie",
        price: 50,
        currency: "PLN"
      },
      {
        id: 25,
        name: "Poi Veils",
        description: "Woale na linkach",
        price: 145,
        currency: "PLN"
      },
      {
        id: 26,
        name: "Poi Veils Jedwab",
        description: "Woal jedwabny na łańcuszku",
        price: 155,
        currency: "PLN"
      },
      {
        id: 27,
        name: "Peacock",
        description: "Wachlarz sceniczny",
        price: 89.90,
        currency: "PLN"
      }
    ]
  },
  {
    id: 3,
    name: "Siatkówka",
    products: [
      {
        id: 31,
        name: "Mikasa",
        description: "Piłka EuroVolleyPoland",
        price: 49.99,
        currency: "PLN"
      },
      {
        id: 32,
        name: "Nike Brasilia",
        description: "Plecak",
        price: 99,
        currency: "PLN"
      },
      {
        id: 33,
        name: "Gel-Kneepad",
        description: "Nakolanniki",
        price: 69.99,
        currency: "PLN"
      },
      {
        id: 34,
        name: "Koszulka MT 14 Tee",
        description: "Koszulka treningowa",
        price: 119,
        currency: "PLN"
      },
      {
        id: 35,
        name: "Core Hoody",
        description: "Bluza adidas",
        price: 169.99,
        currency: "PLN"
      }
    ]
  }
];
