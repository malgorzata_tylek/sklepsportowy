import { Injectable } from '@angular/core';
import { CATEGORIES } from './mockData';
import { Category } from './category';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class MockService {
  getCategoriesData(): Category[] {
    let categoryList: Category[];

    this.getMockData()
    .subscribe((categories) => {
      categoryList = categories;
    });

    return categoryList;
  }

  getMockData(): Observable<Category[]> {
    const categories: Category[] = CATEGORIES;

    return Observable.create( observer => {
      observer.next(categories);
      observer.complete();
    });
  }
}
