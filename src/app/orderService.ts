import { Injectable } from '@angular/core';
import { Product } from './product';

@Injectable()
export class OrderService {
  orderList: Array<Product>;
  private _productCounter: number;
  private _totalPrice: number;

  constructor() {
    this.orderList = new Array();
    this._productCounter = 0;
    this._totalPrice = 0;
  }

  addProduct(product: Product): void {
    this.orderList.push(product);
  }

  getCurrentOrder(): Array<Product> {
    return this.orderList;
  }

  delete(id, amount, price): void {
    this.orderList = this.orderList.filter(product => product.id !== id);
    this._totalPrice -= amount * price;
    this._productCounter -= amount;
  }


  get productCounter(): number {
    return this._productCounter;
  }

  set productCounter(value: number) {
    this._productCounter = value;
  }

  get totalPrice(): number {
    return this._totalPrice;
  }

  set totalPrice(value: number) {
    this._totalPrice = value;
  }
}


