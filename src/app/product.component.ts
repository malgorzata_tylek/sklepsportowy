import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Product } from './product';
import { Utils } from './utils';

@Component({
    selector: 'ProductComponent',
    styleUrls: ['./product.component.css'],
    template:
    `
      <div id="containter">
        <span class="main-section">
          <div class="roboto-light large-font">
            {{ product.name }}
          </div>
          <div class="roboto-light">
            {{ product.description }}
          </div>
        </span>
        <span class="price-container">
          <div class="accented-content roboto-light">
            {{ getFormattedPrice() }}
          </div>
          <button
            class="secondary-accented-content roboto-light"
            (click)="sendShoppingCartUpdate(product)" >
            Dodaj do koszyka
          </button>
        </span>
      </div>
    `
})


export class ProductComponent {
    @Output() updateCartEmitter: EventEmitter<any> = new EventEmitter();
    @Input() product: Product;

    getFormattedPrice(): string {
      return Utils._priceFormatter(this.product.price, this.product.currency);
    }

    sendShoppingCartUpdate(product: Product) {
      this.updateCartEmitter.emit(product);
    }
}
