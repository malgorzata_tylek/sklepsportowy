import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from './product';
import { OrderService } from './orderService';
import { Utils } from './utils';
import { ShoppingCartHeaderComponent } from './shoppingCartHeader.component';
import { Router } from '@angular/router';

class Order {
  id: number;
  amount: number;
  name: string;
  price: number;
  value: number;

  constructor(id: number, amount: number, name: string, price: number, value: number) {
    this.id = id;
    this.amount = amount;
    this.name = name;
    this.price = price;
    this.value = value;
  }
}

@Component({
  selector: `ShoppingCartComponent`,
  styleUrls: ['./shoppingCart.component.css'],
  template:
    `
    <div id="title-header">
        <div>{{title | uppercase}}</div>
        <ShoppingCartHeaderComponent id="shopping-cart" class="shopping-cart-header"> </ShoppingCartHeaderComponent>
    </div>
    <div class="main-container">
      <div id="content">
        <div class="subtitle" >{{ subtitle }}</div>
        <div class="table-container">
          <table>
            <tr>
              <th>Ilość</th>
              <th>Produkt</th>
              <th>Cena</th>
              <th>Wartość</th>
              <th></th>
            </tr>
            <tr *ngFor="let order of orderList"
                [hidden]="deleted[order.id]" >
              <td>{{ order.amount }}</td>
              <td>{{ order.name }}</td>
              <td>{{ order.price }}</td>
              <td>{{ order.value }}</td>
              <td>
                <button
                  (click)="deleteProducts(order)" >
                  Usuń!
                </button>
              </td>
            </tr>
            <tr>
              <td></td><td></td>
              <td>Wartość całkowita:</td>
              <td>{{ getFormattedPrice() }}</td>
            </tr>
          </table>
        </div>
        <div class="buttons">
          <button
            (click)="continueShopping()">
            Kontynuuj zakupy
          </button>
          <button
            (click)="pay()">
            Złóż zamówienie
          </button>
        </div>
      </div>
    </div>
    `
})

export class ShoppingCartComponent implements OnInit {
  @ViewChild(ShoppingCartHeaderComponent) cart: ShoppingCartHeaderComponent;

  title = 'sklep sportowy';
  subtitle = 'Twój koszyk na zakupy:';

  productList: Array<Product>;
  orderList = new Array();

  deleted = {};

  constructor(private router: Router, private orderService: OrderService) {}

  ngOnInit(): void {
    this.productList = this.orderService.getCurrentOrder();
    this.product2Order();
  }

  product2Order(): void {
    let id,
      products;

    for (const product of this.productList) {
      id = product.id;
      products = Utils._filterById(this.productList, id);

      if (Utils._filterById(this.orderList, id).length === 0) {
        this.orderList.push(new Order(id, products.length, product.name, product.price, Utils._sumValue(products)));
      }
    }
  }

  deleteProducts(order: Order) {
    this.deleted[order.id] = true;
    this.orderService.delete(order.id, order.amount, order.price);
    this.cart.synchronizeWithServer();
  }

  getFormattedPrice(): string {
    return Utils._priceFormatter(this.orderService.totalPrice, 'PLN');
  }

  continueShopping(): void {
    this.router.navigate(['/dashboard']);
  }

  pay(): void {
    this.router.navigate(['/form']);
  }
}






