import { Component, Input, OnInit } from '@angular/core';
import { Utils } from './utils';
import { Product } from './product';

import { OrderService } from './orderService';

import { Router } from '@angular/router';

@Component({
    selector: `ShoppingCartHeaderComponent`,
    styleUrls: ['./shoppingCartHeader.component.css'],
    template:
    `
      <div id="containter">
        <span class="right-padding-standard">
          Koszyk:
        </span>
        <span class="right-padding-standard">
          {{productCounter}}
        </span>
        <span class="right-padding-standard">
          produkt(ów),
        </span>
        <span>
          {{ getFormattedPrice() }}
        </span>

        <button class="cash-button"
          (click)="goToCash()">
          Do kasy!
        </button>
      </div>
    `
})

export class ShoppingCartHeaderComponent implements OnInit {
    productCounter: number;
    totalPrice: number;
    currency: string;

    constructor(private router: Router, private orderService: OrderService) {}

    ngOnInit() {
      this.synchronizeWithServer();
      this.currency = 'PLN';
    }

    synchronizeWithServer() {
      this.productCounter = this.orderService.productCounter;
      this.totalPrice = this.orderService.totalPrice;
    }

    updateOrder(product: Product): void {
      this.productCounter += 1;
      this.totalPrice += product.price;

      this.orderService.productCounter = this.productCounter;
      this.orderService.totalPrice = this.totalPrice;
    }

    getFormattedPrice(): string {
      return Utils._priceFormatter(this.totalPrice, this.currency);
    }

    goToCash(): void {
      this.router.navigate(['/cart']);
    }
}
