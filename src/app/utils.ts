import { Product } from './product';

export class Utils {

  static _priceFormatter(price: number, currency: string): string {
    const formattedPrice = price.toFixed(2);
    return formattedPrice + ' ' + currency;
  }

  static _filterById(array, id: number) {
    return array
      .filter(product => product.id === id);
  }

  static _filterById2(array, id: number) {
    return array
      .filter(order => order.id === id);
  }

  static _sumValue(products: Product[]): number {
    return products.reduce((sum, product) => sum + product.price, 0);
  }
}
